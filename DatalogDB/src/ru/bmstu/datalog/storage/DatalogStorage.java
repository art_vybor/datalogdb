package ru.bmstu.datalog.storage;

import java.sql.SQLException;
import java.util.HashSet;

import ru.bmstu.datalog.data.Fact;
import ru.bmstu.datalog.data.Predicate;

/**
 * Is a wrapper for database access.
 * @author art-vybor
 */
public class DatalogStorage {
	private DbAccessor db;
	
	public DatalogStorage(String dbName, boolean inMemory) {
		 db = new DbAccessor(dbName, inMemory);
		 try {
			db.open();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public int size() {
		return db.getNumOfPredicate();
	}
	
	/**
	 * Returns the set of predicate, matching with the input predicate.
	 * @param _predicate
	 * @return {@link HashSet<Predicate>}
	 */
	public HashSet<Predicate> select(Predicate _predicate) {
		HashSet<Predicate> answer = new HashSet<Predicate>();
		
		try {
			answer = db.selectPredicate(_predicate);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return answer;
	}
	
	/**
	 * Add a set of predicate to the storage.
	 * @param hash
	 */
	public void insert(HashSet<Predicate> hash) {
		for (Predicate predicate : hash) {
			insert(predicate);
		}
	}
	
	/**
	 * Add a predicate to the storage.
	 * @param predicate
	 */
	public void insert(Predicate predicate) {
		try {
			db.insertPredicate(predicate);
		} catch (SQLException e) {
			//e.printStackTrace();			
			//TODO переписать insert так, чтобы не было exception'ов, или не надо?
		}
	}
	
	/**
	 * Closes the connection.
	 */
	public void close() {
		try {
			db.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Clears the database.
	 */
	public void clear() {
		try {
			db.clear();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}		
	}

	/**
	 * Add a set of fact to the storage.
	 * @param facts
	 */
	public void insertFacts(HashSet<Fact> facts) {
		for (Fact fact : facts) {
			insert(fact.getPredicate());
		}		
	}
	
	@Override
	public String toString() {
		return db.toString();
	}
}
