package ru.bmstu.datalog.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Realizes the representation Datalog predicate.
 * @author art-vybor
 */
public class Predicate {
	private static Map<Integer, Integer> 	varMap; //map, which is the substitution used in the calculation of the hash function
	private static boolean 			isVarMapInit = false;
	private String name;
	private ArrayList<Argument> args;
	
	public Predicate(String _name, ArrayList<Argument> _args) {
		name = _name;
		args = _args;
	}	
	
	public Predicate() {
		args = new ArrayList<Argument>();
	}
	
	public String getName() {
		return name;
	}
	
	public Argument get(int i) {
		return args.get(i);
	}
	
	public ArrayList<Argument> getArgs() {
		return args;
	}
	
	public int size() {
		return args.size();
	}

	public boolean isSimiliar(Predicate predicate) {
		if (predicate.name.equals(name) && predicate.size() == size()) return true;
		return false;
	}
	
	@Override
	public int hashCode() {
		//TODO придумать более быструю версию
		if (isVarMapInit == false) {
			varMap = new HashMap<Integer, Integer>();
			isVarMapInit = true;
		}
		varMap.clear();
		int index = 0;		
		
		String strForHash = name;
		for (Argument arg : args)
			if (arg.isConstant())
				strForHash += arg.getConstant();
			else {
				if (varMap.containsKey(arg.getVariable())) {
					strForHash += varMap.get(arg.getVariable());
				} else {
					varMap.put(arg.getVariable(), index);
					strForHash += index++;
				}
				 
			}
		
		return strForHash.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		//TODO переписать
		if (!(obj instanceof Predicate))
			return false;

		Predicate predicate = (Predicate) obj;
		
		if (!this.isSimiliar(predicate)) return false;
		
		return this.hashCode() == predicate.hashCode();
	}

	@Override
	public String toString() {
		String resString = name;
		
		resString += "(";
		if (args.size() > 0) {			
			for (Argument arg : args)
				resString += arg + ", ";
			
			resString = resString.substring(0, resString.length() - 2);			
		}		
		resString += ")";
		return resString;
	}
}
