package ru.bmstu.datalog.data;

import java.util.HashSet;

/**
 * Container for datalog program. 
 * @author art-vybor
 */
public class DatalogData {
	private HashSet<Fact> 	 facts;
	private HashSet<Rule> 	 rules;
	private HashSet<Request> requests;

	public DatalogData() {
		facts = new HashSet<Fact>();
		rules = new HashSet<Rule>();
		requests = new HashSet<Request>();
	}
	
	public void addData(DatalogData data) {
		facts.addAll(data.getFacts());
		rules.addAll(data.getRules());
		requests.addAll(data.getRequests());
		
	}
	
	public void addFact(Fact fact) {
		facts.add(fact);
	}

	public void addRule(Rule rule) {
		rules.add(rule);
	}
	
	public void addRequest(Request request) {
		requests.add(request);
	}

	public HashSet<Fact> getFacts() {
		return facts;
	}

	public HashSet<Rule> getRules() {
		return rules;
	}
	
	public HashSet<Request> getRequests() {
		return requests;
	}
	
	@Override
	public String toString() {
		String resString = "";
		for (Fact fact : facts)
			resString += fact + "\n";

		for (Rule rule : rules)
			resString += rule + "\n";
		
		for (Request request : requests)
			resString += request + "\n";
		
		if (resString.length() > 0)
			return resString.substring(0, resString.length() - 1);
		else
			return resString;
	}
}
