package ru.bmstu.datalog.data;

/**
 * Realizes the representation Datalog request.
 * @author art-vybor
 */
public class Request {
	private Predicate predicate;

	public Predicate getPredicate() {
		return predicate;
	}

	public Request(Predicate _predicate) {
		predicate = _predicate;
	}
	
	@Override
	public String toString() {
		return "? :- " + predicate + '.';
	}
}
