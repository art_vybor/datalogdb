package ru.bmstu.datalog.data;

/**
 * Realizes the representation argument of datalog argument.
 * @author art-vybor
 */
public class Argument {
	private String	constant;
	private int	variable;
	private boolean	isConstant;
	
	public Argument(Argument arg) {
		constant = arg.getConstant();
		variable = arg.getVariable();
		isConstant = arg.isConstant();
	}
	
	public Argument(String _constant) {
		isConstant = true;		
		constant = _constant;		
	}
	
	public Argument(String _constant, int _variable) {
		isConstant = false;
		variable = _variable;
		constant = _constant;
	}
	
	public String getConstant() {
		return constant;
	}
	public void setConstant(String _constant) {
		constant = _constant;
	}
	public int getVariable() {
		return variable;
	}
	public void setVariable(int _variable) {
		variable = _variable;
	}
	public boolean isConstant() {
		return isConstant;
	}
	public boolean isVariable() {
		return !isConstant;
	}	
		
	@Override
	public boolean equals(Object obj) {
		return this.toString().equals(((Argument)obj).toString());
	}
	
	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	@Override
	public String toString() {
		if (isConstant || variable == -1 && constant != null) return constant;
		else return ((Integer)variable).toString();		
	}
}
