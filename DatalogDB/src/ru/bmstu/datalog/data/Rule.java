package ru.bmstu.datalog.data;

import java.util.ArrayList;

/**
 * Realizes the representation Datalog rule.
 * @author art-vybor
 */
public class Rule {
	private Predicate head;
	private ArrayList<Predicate> body;
	
	public Rule(Predicate _head, ArrayList<Predicate> _body) {
		head = _head;
		body = _body;
	}

	public Rule() {
		body = new ArrayList<Predicate>();
	}
	
	public Predicate getHead() {
		return head;
	}
	
	public void setHead(Predicate _head) {
		head = _head;
	}
	
	public void addToBody(Predicate predicate) {
		body.add(predicate);
	}

	@Override
	public String toString() {
		String resString = head + " :- ";
		
		for (Predicate predicate : body)
			resString += predicate.toString() + ", ";

		resString = resString.substring(0, resString.length() - 2);

		return resString + ".";
	}

	public ArrayList<Predicate> getBody() {
		return body;
	}
}
