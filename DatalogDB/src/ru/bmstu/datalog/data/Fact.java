package ru.bmstu.datalog.data;

import java.util.List;

/**
 * Realizes the representation Datalog fact.
 * @author art-vybor
 */
public class Fact {
	Predicate predicate;
	
	public Fact() {
		predicate = new Predicate();
	}
	
	public Fact(Predicate _fact) {
		predicate = _fact;
	}
	
	public String getName() {
		return predicate.getName();
	}

	public int size() {
		return predicate.size();
	}

	public List<Argument> getArgs() {
		return predicate.getArgs();
	}
	
	public Predicate getPredicate() {
		return predicate;
	}
	
	@Override
	public String toString() {
		return predicate + ".";
	}
}
