package ru.bmstu.datalog.algo;

import ru.bmstu.datalog.data.Predicate;

/**
 * Represent id for predicate.
 * @author art-vybor
 */
class PredicateIdentifier {
	private String 	name;
	private int	size;
	
	public PredicateIdentifier(String _name, int _size) {
		name = _name;
		size = _size;
	}
	
	public PredicateIdentifier(Predicate predicate) {
		name = predicate.getName();
		size = predicate.size();
	}

	public int getSize() {
		return size;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PredicateIdentifier))
			return false;

		PredicateIdentifier predicateId = (PredicateIdentifier) obj;
		
		if (predicateId.getName().equals(name) && predicateId.getSize() == size) return true;
		return false;
	}
	
	@Override
	public int hashCode() {
		return (name + ((Integer)size).toString()).hashCode();
	}
	
	@Override
	public String toString() {
		return "[" + name + " " + ((Integer)size).toString() + "]";
	}
}
